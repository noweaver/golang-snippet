package main

import (
	"strings"
	"fmt"
)

func main() {
	addZip := makeSuffix(".zip")
	addTgz := makeSuffix(".tar.gz")
	fmt.Println(addTgz("go.1.10.1.src"))
	fmt.Println(addZip("go.1.10.1.mac-amd64"))
}

func makeSuffix(suffix string) func(string) string {
	return func(name string) string {
		if !strings.HasSuffix(name, suffix) {
			return name + suffix
		}

		return name
	}
}
