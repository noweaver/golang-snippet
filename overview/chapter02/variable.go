package main

import "fmt"

func main() {
	var a int

	a = 10
	fmt.Println(a)

	// 여러 개 변수 선언
	var name, id, address string
	name = "Ryan"
	id = "nowist"
	address = "korea"

	fmt.Println(name + ":" + id + ":" + address)

	// 가독성을 위해서 여러 변수 그룹으로 선언
	var (
		name2 string
		age2 int
		weight2 float32
	)

	name2 = "ryan"
	age2 = 43
	weight2 = 64.5

	fmt.Println(name2)
	fmt.Println(age2)
	fmt.Println(weight2)

	// 변수 타입 생략
	// var 와 타입 생략
	var c = true
	fmt.Println(c)

	// var, type 생략
	// := 를 사용
	d := false
	fmt.Println(d)
}
