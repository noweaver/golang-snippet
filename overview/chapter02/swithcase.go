package main

import "fmt"

func main() {
	c := 'a'

	// case 에 조건식 사용
	switch {
	case '0' <= c && c <= '9':
		fmt.Println("%c 은(는) 숫자 입니다.", c)

	case 'a' <= c && c <= 'z':
		fmt.Println("%c 은(는) 소문자 입니다.", c)

	case 'A' <= c && c <= 'Z':
		fmt.Println("%c 은(는) 대문자 입니다.", c)
	}


	// 조건이 맞으면 바로 빠져 나온다. break 를 사용하지 않아도 된다.
	i := 2
	switch i {
	case -1, -2:
		fmt.Println(i, "는 음수입니다.")
	case 1, 2:
		fmt.Println(i, "는 양수입니다.")
	}

	// fallthrough
	i = 0
	switch i {
	case 0:
		fallthrough
	case 1:
		fallthrough
	case 2:
		fallthrough
	case 3:
		fallthrough
	case 4:
		fallthrough
	case 5:
		f()
	}

	// fallthrough2
	i = 2
	switch i {
	case 1:
		fmt.Println("i는 1보다 작거나 같습니다.")
		fallthrough
	case 2:
		fmt.Println("i는 2보다 작거나 같습니다.")
		fallthrough
	case 3:
		fmt.Println("i는 3보다 작거나 같습니다.")
	}


	// 조건 없는 switch-case
	// if-else 와 매우 비슷, if-else 를 많이 사용해야 한다면 switch 구문이 훨씬 좋다.
	i = -2
	switch {
	case i < 0:
		fmt.Println(i, "는 음수입니다.")
	case i == 0:
		fmt.Println(i, "는 0입니다.")
	case i > 0:
		fmt.Println(i, "는 양수입니다.")
	}

	// 초기값을 만들 수 있다. 다만 초기값 끝네 세미콜론 사용해야 함
	switch a, b := 1, 2; {
	case a < b:
		fmt.Println("a는 b보다 작습니다.")
	case a == b:
		fmt.Println("a와 b는 같습니다.")
	case a > b:
		fmt.Println("a는 b보다 큽니다.")
	}
}

func f() {
	fmt.Println("I am f function")
}
