package pkg2

import (
	"fmt"
	"golang-snippet/overview/chapter02/pkg3"
)

func init() {
	fmt.Println("===== pkg2.init() =====")
	pkg3.MyShow3()
}

func MyShow2()  {
	fmt.Println("----- pkg2.MyShow2() -----")
}