package pkg2

import (
	"fmt"
	"golang-snippet/overview/chapter02/lib"
)

var v rune

func init()  {
	v = '1'
}

func main() {
	fmt.Println(lib.IsDigit(v))
}

func IsDigit(c int32) bool {
	return '0' <= c && c <= '9'
}
