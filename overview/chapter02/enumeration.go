package main

import "fmt"

//const (
//	Sunday = 0
//	Monday = 1
//	Tuesday = 2
//	Thursday = 3
//	Friday = 4
//	Saturday = 5
//)
//
//const (
//	Sunday = iota
//	Monday
//	Tuesday
//	Thursday
//	Friday
//	Saturday
//)


//type Color int
//const (
//	RED Color = iota
//	ORANGE
//	YELLOW
//	GREEN
//)


//type ByteSize int64
//const (
//	//- iota
//	KB ByteSize =1 << (10 * iota)	// 1 << (10 * 1) = 1024
//	MB								// 1 << (10 * 2) = 1048576
//	GB 								// 1 << (10 * 3) = 1073741824
//	TB								// 1 << (10 * 4) = 1099511627776
//	PB								// 1 << (10 * 5) = 1125899906842624
//	EB								// 1 << (10 * 6) = 1152921504606846976
//)

//const (
//	DEFAULT RATE = 5 + 0.3 * iota	// 5
//	GREEN_RATE						// 5.3
//	SILVER_RATE						// 5.6
//	GOLD_RATE						// 5.9
//)


const (
	Running = 1 << iota		// 1 << 0 == 1
	Waiting					// 1 << 1 == 2
	Send					// 1 << 2 == 4
	Receive					// 1 << 3 == 8
)


func main() {
	// OR 연산자(|)로 stat 변수 생성
	stat := Running | Send
	display(stat)
}

func display(stat int) {
	// AND 연산자(&)로 stat에 포함된 비트 값의 상태 출력
	if stat & Running == Running {
		fmt.Println("Running")
	}

	if stat & Waiting == Waiting {
		fmt.Println("Waiting")
	}

	if stat & Send == Send {
		fmt.Println("Send")
	}

	if stat & Receive == Receive {
		fmt.Println("Receive")
	}
}
