package main

import "fmt"

func main() {

	fmt.Println("====== call by value =====")
	i := 10
	inc(i)
	fmt.Println(i)


	fmt.Println("====== call by reference =====")
	inc2(&i)
	fmt.Println(i)
	
}

func inc2(i *int) {
	*i = *i + 1
}

func inc(i int) {
	i = i + 1
}