package pkg3

import "fmt"

func init() {
	fmt.Println("===== pkg3.init() =====")
}

func MyShow3()  {
	fmt.Println("----- pkg3.MyShow3() -----")
}