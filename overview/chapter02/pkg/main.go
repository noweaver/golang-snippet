package pkg

import (
	"fmt"

	mylib "golang-snippet/overview/chapter02/lib"
	//_ "golang-snippet/overview/chapter02/lib"

)

//
// 실행가능한 패키지 테스트
//

// 실행가능한 package 는 package 이름이 main 이어야 한다.
// package 이름이 main 이면 실행가능한 패키지가 된다.
//
// main 패키지를 빌드하면 디렉터리 이름과 같은 이름으로 실행파일이 생성 된다.
//
// 프로그램을 실행하면 main 패키지의 main() 함수를 entry-point 로 찾는다.
func main() {
	fmt.Println("Hello")

	// 라이브러리 패키지
	// 라이브러리 패키지 내의 함수, 변수, 상수, 사용자 정의 타입, 메서드, 구조체 필드 등의 signature 가 대문자면 패키지 외부에서 접근 가능
	// fmt.Println(lib.IsDigit('1'))
	fmt.Println(mylib.IsDigit('1'))
	fmt.Println(mylib.IsDigit('a'))

	// 라이브러리 패키지
	// 소문자로 시작하기 때문에 외부에서 호출이 불가능	// fmt.Println(lib.isSpace('\t'))


	// 매우 긴 라이브러리 패키지 앨리어스
	fmt.Println(mylib.IsTrue(100))

}
