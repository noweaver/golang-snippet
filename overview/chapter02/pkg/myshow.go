package pkg

import (
	"fmt"
	"golang-snippet/overview/chapter02/pkg2"
)

func init() {
	fmt.Println("===== pkg.init() =====")
	pkg2.MyShow2()
}

func MyShow()  {
	fmt.Println("----- pkg.MyShow() -----")
}