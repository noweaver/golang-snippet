package main

import (
	"fmt"
	"strconv"
)

func main() {
	//
	// 반환 값이 1개일 때
	i := maxFunc(100, 200)
	fmt.Println(i)

	//
	// 반환 값이 2개 일 때
	a := 100
	b := 200

	fmt.Println("Before swapping a = ", a, "b = ", b)
	a, b = swap(a, b)
	fmt.Println("After swapping a = ", a, "b = ", b)

	//
	// 리턴 2개 이상의 함수 에러처리
	fmt.Println("\n====== 리턴 2개 이상의 함수 에러처리 ======")
	myDisplay("two")
	myDisplay("2")

	//
	// return value 에 이름 생성
	fmt.Println("\n====== return value 에 이름 생성 ======")
	retValue := myAdd(10, 5)
	fmt.Println(retValue)

	// 함수를 매개변수로 전달하기
	fmt.Println("\n====== 함수를 매개변수로 전달하기 ======")
	callback(1, add) // 1 + 2 = 3
}

func callback(i int, f func(int, int)) {
	f(i, 2) // add(1, 2)를 호출
}

func add(a, b int) {
	fmt.Printf("%d + %d = %d\n", a, b, a+b) // 1+2 = 3
}

func myAdd(a, b int) (retValue int) {
	retValue = a + b
	return
}

func myDisplay(str string) {
	if v, err := strconv.Atoi(str); err != nil {
		fmt.Println(str, "은 숫자형 문자열이 아닙니다.")
	} else {
		fmt.Printf("정수 값 %d", v)
	}

}

func swap(a int, b int) (int, int) {
	temp := a
	a = b
	b = temp

	return a, b
}

func maxFunc(a, b int) int {
	max := a
	if b > max {
		max = b
	}

	return max
}