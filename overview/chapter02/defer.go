package main

import "fmt"

func main() {
	fmt.Println("===== defer 기본 사용 ======")
	func1()

	fmt.Println("===== defer stack 특징 =====")
	deferStack()

	fmt.Println("\n====== 트레이스 로그 출력 =====")
	b()
}

func b() {
	enter("b")
	defer leave("b")
	fmt.Println("in b")
	a()
}
func a() {
	enter("a")
	defer leave("a")
	fmt.Println("in a")
}

func leave(s string) {
	fmt.Println("leaving: ", s)
}

func enter(s string) {
	fmt.Println("entering: ", s)
}

func deferStack() {
	for i := 0; i < 10; i++ {
		defer fmt.Print(i, ",")
	}
}

func func1() {
	fmt.Println("#1")
	defer func2()
	fmt.Println("#2")
}
func func2() {
	fmt.Println("func2")
}
