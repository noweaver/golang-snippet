package lib

//
// 라이브러리 패키지 테스트
//

// main 패키지 외에는 모두 라이브러리로 만들어진다.
// Go 기본 라이브러리 패키지가 아닌 커스텀 패키지를 임포트할 때는 $GOPATH/src 디렉터리 기준으로 하는 경로를 Import 해야 함

// 패키지.식별자 로 접근

func IsDigit(c int32) bool {
	return '0' <= c && c <= '9'
}

func isSpace(c int32) bool {
	switch c {
	case '\t', '\n', '\v', '\f', '\r', 0x85, 0xA0:
		return true
	}

	return false
}