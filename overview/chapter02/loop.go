package main

import "fmt"

func main() {

	// for 문에 초기화 구문, 조건식, 후속 작업 정의
	loop1()

	// for 문에 조건식만 사용
	loop2()

	// for 문에 조건식 생략
	loop3()

	// for 문에 레이블 사용
	loop4()

	// for 문에 continue + label 사용
	loop5()
}

func loop5() {
	x := 5
	table := [][]int {
		{1, 5, 9},
		{2, 6, 5, 13},
		{5, 3, 7, 4},
	}

	fmt.Println("===== continue & label loop =====")
	LOOP:
	for row, rowValue := range table {
		//fmt.Println("TEST: ", row)
		for col, colValue := range rowValue {
			fmt.Println("count: ", col)
			if colValue == x {
				fmt.Printf("found %d(row: %d, col: %d)\n", x, row, col)
				fmt.Println("Before continue loop")
				// LOOP 로 지정된 for 문의 다음 반복문 수행
				// 찾으면 for 을 끝까지 돌지 않고 continue 에 따라서 다음 카운트의 loop 를 돌 것 같지만 LOOP 라벨 때문에 처음의 loop 로 간다.
				continue LOOP
			}
		}
	}
}

func loop4() {
	x := 7
	table := [][]int {
		{1, 5, 9},
		{2, 6, 5, 13},
		{5, 3, 7, 4},
	}

	found := false
	for row := 0; row < len(table); row++ {
		for col := 0; col < len(table[row]); col++ {
			if table[row][col] == x {
				found = true
				fmt.Printf("found %d(row: %d, col: %d)\n", x, row, col)
				break
			}
		}

		if found {
			break
		}
	}

	LOOP:
	for row := 0; row < len(table); row++ {
		for col := 0; col < len(table[row]); col++ {
			if table[row][col] == x {
				fmt.Printf("found %d(row: %d, col: %d)\n", x, row, col)
				break LOOP
			}
		}
	}

	//
	// range 활용
	//
	fmt.Println("===== range + label =====")
	LOOP2:
	for row, rowValue := range table {
		fmt.Println("row = ", row, "rowValue = ", rowValue)
		for col, colValue := range rowValue {
			fmt.Println("col = ", col, "colValue = ", colValue)
			if colValue == x {
				fmt.Printf("found %d(row: %d, col: %d)\n", x, row, col)
				break LOOP2
			}
		}
	}
}

func loop1() {
	sum := 0

	// for 문에 초기화 구문, 조건식, 후속 작업 정의
	for i := 1; i <= 10; i++ {
		sum += i
	}

	fmt.Println("Loop1")
	fmt.Println(sum)
}

func loop2() {
	sum, i := 0, 1

	// for 문에 조건식만 사용
	for i <= 10 {
		sum += i
		i++
	}

	fmt.Println("\nLoop2")
	fmt.Println(sum)
}

func loop3() {
	var sum = 0
	var i = 1
	//sum, i := 0, 1
	// for 문에 조건식 생략
	for {
		if i >= 11 {
			break
		}
		sum += i
		i++
	}

	fmt.Println("\nLoop3")
	fmt.Println(sum)
}
