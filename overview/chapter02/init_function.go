package main

import (
	"golang-snippet/overview/chapter02/pkg"
	"fmt"
)

func init() {
	pkg.MyShow()
}

func main() {
	fmt.Println("Hello")
}
